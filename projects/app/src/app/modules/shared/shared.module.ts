import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './components/footer/footer.component';
import { PlayerCardComponent } from './components/player-card/player-card.component';
import { CounterComponent } from './components/counter/counter.component';

@NgModule({
  imports: [
    // Modules
    CommonModule,
    RouterModule,
  ],
  declarations: [HeaderComponent, FooterComponent, PlayerCardComponent, CounterComponent],
  exports: [HeaderComponent, FooterComponent, PlayerCardComponent, CounterComponent],
})
export class SharedModule {}
