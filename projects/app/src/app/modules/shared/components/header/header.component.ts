import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../core/services/auth.service';
import { ToastService, ToastType } from '@lenne.tech/ng-base/shared';
import { DataService } from '../../../core/services/data.service';
import { Data } from '../../../core/models/data.interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  data: Data;

  constructor(
    private authService: AuthService,
    private router: Router,
    private toastService: ToastService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.dataService.data.subscribe(data => {
      this.data = data;
    });
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['/auth']);
    this.toastService.show(
      {
        id: 'logout-toast',
        type: ToastType.SUCCESS,
        title: 'Erfolgreich',
        description: 'Erfolgreich ausgeloggt',
      },
      2000
    );
  }
}
