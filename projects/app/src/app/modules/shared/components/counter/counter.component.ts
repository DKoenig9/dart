import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Player } from '../../../core/models/player.interface';
import { DataService } from '../../../core/services/data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
})
export class CounterComponent {
  @Input() currentPlayer: Player;
  @Input() players: Player[] = [];
  faktor = 1;
  currentDart = 0;
  startValue: number;

  alreadySaved = false;

  @Output() gameWon: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() changeCurrentDart: EventEmitter<number> = new EventEmitter<number>();
  @Output() changeCurrentPlayer: EventEmitter<Player> = new EventEmitter<Player>();

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute
  ) {}

  onDart(number: number, faktor: number) {
    // Punkte entsprechen der geworfenen Zahl mal den Faktor
    const points = number * faktor;

    // Beim ersten Wurf einer Runde wird die startValue gespeichert, um sie beim Überwerfen nutzen zu können
    if (this.currentDart === 0) {
      this.startValue = this.currentPlayer.points;
    }

    // Die Punkte des geworfenen Darts werden von den Punkten des Spielers abgezogen
    this.currentPlayer.points = this.currentPlayer.points - points;

    // Der geworfene Dart wird als Objekt im Spieler abgespeichert
    this.currentPlayer.darts.push({ number, faktor, points });

    // Wenn der Spieler 100 Darts geworfen hat, ist das AVG - Training vorbei und wird gespeichert
    if (this.route.snapshot.url[1].path === 'avg' && this.currentPlayer.darts.length === 100) {
      this.gameWon.emit(true);
      // TODO: Speichern
      // this.dataService.saveTraining(this.player);
    }

    // Wenn der Spieler per DoubleOut checkt, ist das Spiel gewonnen und wird gespeichert
    if (this.currentPlayer.points === 0 && this.faktor === 2) {
      // TODO: Speichern
      if (!this.alreadySaved) {
        this.dataService.saveGame(this.players);
      }
      if (this.players.length > 1 && confirm('Weiter spielen?')) {
        this.players.splice(this.players.indexOf(this.currentPlayer), 1);
        this.alreadySaved = true;
        this.setNextPlayer();
      } else {
        this.gameWon.emit(true);
      }
      // this.dataService.saveTraining(this.player);
      // Wenn der Spieler unter 2 Punkte kommt, werden die Werte auf den Anfang der entsprechenden Runde resettet und der nächste Spieler ausgewählt
    } else if (this.currentPlayer.points < 2) {
      this.currentPlayer.points = this.startValue;
      this.faktor = 1;
      this.setNextPlayer();
      return;
    }

    // Wenn der letzte der 3 Darts geworfen wurde, wird der Durchschnitt neu berechnet und der nächste Spieler ausgewählt
    if (this.currentDart === 2) {
      let sum = 0;
      this.currentPlayer.darts.forEach(dart => {
        sum = sum + dart.number * dart.faktor;
      });
      this.currentPlayer.avg = (sum / this.currentPlayer.darts.length) * 3;

      // Die Werte des currentPlayers werden im Players-Array gespeichert und der nächste Spieler wird ausgewählt
      this.setNextPlayer();

      // Wenn es noch nicht der letzte der 3 Darts war, wird der currentDart um 1 erhöht
    } else {
      this.currentDart++;
      this.changeCurrentDart.emit(this.currentDart);
    }

    // Zum Schluss einer jeden Runde wird der Faktor auf 1 zurückgesetzt
    this.faktor = 1;
  }

  setNextPlayer() {
    this.currentDart = 0;
    this.changeCurrentDart.emit(this.currentDart);
    this.startValue = 0;
    if (this.players[this.players.indexOf(this.currentPlayer) + 1]) {
      this.currentPlayer = this.players[this.players.indexOf(this.currentPlayer) + 1];
      this.changeCurrentPlayer.emit(this.currentPlayer);
    } else {
      this.currentPlayer = this.players[0];
      this.changeCurrentPlayer.emit(this.currentPlayer);
    }
  }

  onBack() {
    this.currentPlayer.points =
      this.currentPlayer.points + this.currentPlayer.darts[this.currentPlayer.darts.length - 1].points;
    this.currentPlayer.darts.pop();
    if (this.currentDart === 0) {
      this.currentDart = 2;
      this.changeCurrentDart.emit(this.currentDart);
    } else {
      this.currentDart--;
      this.changeCurrentDart.emit(this.currentDart);
    }
  }

  onSkip() {
    if (this.currentDart !== 0) {
      this.currentPlayer.points = this.startValue;
    }
    this.setNextPlayer();
  }
}
