import { Injectable } from '@angular/core';
import { Data } from '../models/data.interface';
import { StorageService } from '@lenne.tech/ng-base/shared';
import { BehaviorSubject } from 'rxjs';
import { Player } from '../models/player.interface';
import { Training } from '../models/training.interface';
import { TrainingData } from '../models/training-data.interface';
import { Around } from '../models/around.interface';
import { AroundData } from '../models/around-data.interface';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  data: BehaviorSubject<Data> = new BehaviorSubject<Data>({ users: [] });
  trainingData: BehaviorSubject<TrainingData> = new BehaviorSubject<TrainingData>({ users: [] });
  aroundData: BehaviorSubject<AroundData> = new BehaviorSubject<AroundData>({ users: [] });

  constructor(private storageService: StorageService) {
    this.getData();
  }

  getData() {
    const storageData = this.storageService.load('dartData');
    const storageTrainingData = this.storageService.load('trainingData');
    const storageAroundData = this.storageService.load('aroundData');
    if (storageData) {
      this.data.next(storageData);
    }
    if (storageTrainingData) {
      this.trainingData.next(storageTrainingData);
    }
    if (storageAroundData) {
      this.aroundData.next(storageAroundData);
    }
  }

  addPlayer(name: string) {
    this.data.value.users.push({ name, games: [] });
    this.trainingData.value.users.push({ name, trainings: [] });
    this.storageService.save('dartData', this.data.value);
    this.storageService.save('trainingData', this.trainingData.value);
  }

  saveGame(players: Player[]) {
    players.forEach(player => {
      const game: any = {
        darts: player.darts,
        won: player.points === 0,
        remainingPoints: player.points,
        avg: player.avg,
      };
      this.data.value.users.find(user => {
        if (user.name === player.name) {
          user.games.push(game);
        }
      });
    });

    this.storageService.save('dartData', this.data.value);
  }

  saveTraining(player: Player) {
    const training: Training = {
      darts: player.darts,
      avg: player.avg,
    };

    this.trainingData.value.users.find(user => {
      if (user.name === player.name) {
        user.trainings.push(training);
      }
    });
    this.storageService.save('trainingData', this.trainingData.value);
  }

  saveAround(game: Around[], player: string) {
    const data = this.storageService.load('aroundData');
    if (data) {
      if (!data.users.find((user: { name: string; games: any }) => user.name === player)) {
        data.users.push({ name: player, games: [game] });
        this.storageService.save('aroundData', data);
      } else {
        data.users.find((user: { name: string; games: any }) => {
          if (user.name === player) {
            user.games.push(game);
          }
        });
        this.storageService.save('aroundData', data);
      }
    } else {
      this.storageService.save('aroundData', { users: [{ name: player, games: [game] }] });
    }
  }
}
