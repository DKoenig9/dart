import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StorageService } from '@lenne.tech/ng-base/shared';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isAuth: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private storageService: StorageService) {
    this.isAuth.next(this.storageService.load('auth'));
  }

  getAuthStatus() {
    return this.isAuth.value;
  }

  login() {
    this.isAuth.next(true);
    this.storageService.save('auth', true);
  }

  logout() {
    this.isAuth.next(false);
    this.storageService.remove('auth');
  }
}
