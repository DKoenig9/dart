import { Dart } from './dart.interface';

export interface Player {
  name: string;
  points: number;
  avg: number;
  darts: Dart[];
}
