import { User } from './user.interface';

export interface Data {
  users: User[];
}
