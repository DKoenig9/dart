import { Dart } from './dart.interface';

export interface Game {
  darts: Dart[];
  won: boolean;
  avg: number;
  remainingPoints: number;
}
