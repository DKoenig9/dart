import { AroundUser } from './around-user.interface';

export interface AroundData {
  users: AroundUser[];
}
