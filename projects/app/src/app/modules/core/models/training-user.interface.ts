import { Training } from './training.interface';

export interface TrainingUser {
  name: string;
  trainings: Training[];
}
