import { Around } from './around.interface';

export interface AroundUser {
  name: string;
  games: Around[][];
}
