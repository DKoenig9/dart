import { Dart } from './dart.interface';

export interface VsStats {
  thrownDarts: number;
  overallAvg: number;
  avgBeforeCheckout?: number;
  favNumber: number;
  favNumberCount: number;
  favTriple: number;
  favTripleCount: number;
  thrownDartAvg: number;
  minThrownDart: number;
  maxThrownDart: number;
  gamesOverall: number;
  bestCheckout: { points: number; checkout: Dart[] };
  favOpponent: string;
  hateOpponent: string;
  wins: number;
  losses: number;
  highestWin: { opponent: string; points: number };
  highestLoss: {
    opponent: string;
    points: number;
  };
}
