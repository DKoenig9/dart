import { TrainingUser } from './training-user.interface';

export interface TrainingData {
  users: TrainingUser[];
}
