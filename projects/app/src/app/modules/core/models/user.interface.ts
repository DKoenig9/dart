import { Game } from './game.interface';

export interface User {
  name: string;
  games: Game[];
}
