import { Dart } from './dart.interface';

export interface TrainingStats {
  thrownDarts: number;
  overallAvg: number;
  avgBeforeCheckout?: number;
  favNumber: number;
  favNumberCount: number;
  favTriple: number;
  favTripleCount: number;
  thrownDartAvg: number;
  minThrownDart: number;
  maxThrownDart: number;
  gamesOverall: number;
  bestCheckout: { points: number; checkout: Dart[] };
}
