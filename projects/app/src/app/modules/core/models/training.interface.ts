import { Dart } from './dart.interface';

export interface Training {
  darts: Dart[];
  avg: number;
}
