import { Component } from '@angular/core';
import { AuthService } from '../../modules/core/services/auth.service';
import { Router } from '@angular/router';
import { ToastService, ToastType } from '@lenne.tech/ng-base/shared';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent {
  constructor(private authService: AuthService, private router: Router, private toastService: ToastService) {}

  onLogin() {
    this.authService.login();
    this.router.navigate(['/dashboard']);
    this.toastService.show(
      {
        id: 'logout-toast',
        type: ToastType.SUCCESS,
        title: 'Erfolgreich',
        description: 'Erfolgreich eingeloggt',
      },
      2000
    );
  }
}
