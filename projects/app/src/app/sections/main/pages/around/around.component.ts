import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../../../../modules/core/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { Around } from '../../../../modules/core/models/around.interface';

@Component({
  selector: 'app-around',
  templateUrl: './around.component.html',
  styleUrls: ['./around.component.scss'],
})
export class AroundComponent implements OnInit {
  numbers: number[] = [20, 1, 18, 4, 13, 6, 10, 15, 2, 17, 3, 19, 7, 16, 8, 11, 14, 9, 12, 5];
  currentNumber = 0;
  gameStarted = false;
  numberCountForm: FormGroup;
  game: Around[] = [];

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.numberCountForm = new FormGroup({
      number: new FormControl(null, [Validators.required]),
    });
  }

  startGame() {
    this.currentNumber = 1;
    this.gameStarted = true;
  }

  nextNumber() {
    this.game.push({ number: this.currentNumber, count: this.numberCountForm.value.number });
    this.currentNumber++;
    this.numberCountForm.reset();
    if (this.currentNumber > 20) {
      this.dataService.saveAround(this.game, this.route.snapshot.queryParams['player']);
    }
  }

  getTotalCount(): number {
    let totalCount = 0;
    this.game.forEach(number => {
      totalCount = totalCount + number.count;
    });
    return totalCount;
  }
}
