import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Player } from '../../../../modules/core/models/player.interface';

@Component({
  selector: 'app-vs',
  templateUrl: './vs.component.html',
  styleUrls: ['./vs.component.scss'],
})
export class VsComponent implements OnInit {
  currentPlayer: Player;
  currentDart = 0;
  players: Player[] = [];
  finished = false;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.route.queryParams.subscribe(query => {
        const arr = query['players'].toString().split(',');
        arr.forEach((player: string) => {
          this.players.push({ name: player, points: param['id'], avg: 0, darts: [] });
        });
        this.currentPlayer = this.players[0];
      });
    });
  }

  gameWon() {
    this.finished = true;
  }
}
