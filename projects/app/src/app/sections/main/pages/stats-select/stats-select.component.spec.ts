import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsSelectComponent } from './stats-select.component';

describe('StatsSelectComponent', () => {
  let component: StatsSelectComponent;
  let fixture: ComponentFixture<StatsSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StatsSelectComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(StatsSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
