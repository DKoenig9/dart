import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../modules/core/services/data.service';
import { User } from '../../../../modules/core/models/user.interface';
import { StorageService } from '@lenne.tech/ng-base/shared';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-stats-select',
  templateUrl: './stats-select.component.html',
  styleUrls: ['./stats-select.component.scss'],
})
export class StatsSelectComponent implements OnInit {
  users: User[] = [];
  addForm: FormGroup;

  constructor(
    private dataService: DataService,
    private storageService: StorageService
  ) {}

  ngOnInit(): void {
    this.dataService.data.subscribe(data => {
      this.users = data.users;
    });
    this.addForm = new FormGroup({ player: new FormControl(null, Validators.required) });
  }

  addPlayer() {
    this.dataService.addPlayer(this.addForm.value.player);
    this.addForm.reset();
  }
}
