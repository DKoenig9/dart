import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../modules/core/services/data.service';
import { User } from '../../../../modules/core/models/user.interface';

@Component({
  selector: 'app-vs-select',
  templateUrl: './vs-select.component.html',
  styleUrls: ['./vs-select.component.scss'],
})
export class VsSelectComponent implements OnInit {
  users: User[] = [];
  players: string[] = [];

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.dataService.data.subscribe(data => {
      this.users = data.users;
    });
  }

  selectUser(user: User) {
    this.players.push(user.name);
    console.log(this.players);
  }

  setParams() {
    return { players: [this.players] };
  }
}
