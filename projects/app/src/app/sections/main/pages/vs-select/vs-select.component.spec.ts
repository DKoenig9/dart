import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VsSelectComponent } from './vs-select.component';

describe('VsSelectComponent', () => {
  let component: VsSelectComponent;
  let fixture: ComponentFixture<VsSelectComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VsSelectComponent],
    });
    fixture = TestBed.createComponent(VsSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
