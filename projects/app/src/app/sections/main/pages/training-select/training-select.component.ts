import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../modules/core/services/data.service';
import { TrainingUser } from '../../../../modules/core/models/training-user.interface';

@Component({
  selector: 'app-training-select',
  templateUrl: './training-select.component.html',
  styleUrls: ['./training-select.component.scss'],
})
export class TrainingSelectComponent implements OnInit {
  users: TrainingUser[] = [];
  players: any;
  selected: string;

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.dataService.trainingData.subscribe(data => {
      this.users = data.users;
    });
  }

  selectUser(user: TrainingUser) {
    this.selected = user.name;
    this.players = user.name;
  }
}
