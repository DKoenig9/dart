import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingSelectComponent } from './training-select.component';

describe('TrainingSelectComponent', () => {
  let component: TrainingSelectComponent;
  let fixture: ComponentFixture<TrainingSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TrainingSelectComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TrainingSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
