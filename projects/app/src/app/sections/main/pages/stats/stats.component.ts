import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../modules/core/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../../modules/core/models/user.interface';
import { TrainingUser } from '../../../../modules/core/models/training-user.interface';
import { TrainingStats } from '../../../../modules/core/models/training-stats.interface';
import { VsStats } from '../../../../modules/core/models/vs-stats.interface';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss'],
})
export class StatsComponent implements OnInit {
  vs: User;
  training: TrainingUser;

  trainingStats: TrainingStats = {
    thrownDarts: 0,
    overallAvg: 0,
    favNumber: 0,
    favNumberCount: 0,
    favTriple: 0,
    //TODO:  avgBeforeCheckout: 0;
    favTripleCount: 0,
    thrownDartAvg: 0,
    minThrownDart: 0,
    maxThrownDart: 0,
    gamesOverall: 0,
    bestCheckout: { points: 0, checkout: [] },
  };

  vsStats: VsStats = {
    thrownDarts: 0,
    overallAvg: 0,
    favNumber: 0,
    favNumberCount: 0,
    favTriple: 0,
    // avgBeforeCheckout: 0
    favTripleCount: 0,
    thrownDartAvg: 0,
    minThrownDart: 0,
    maxThrownDart: 0,
    gamesOverall: 0,
    bestCheckout: { points: 0, checkout: [] },
    wins: 0,
    losses: 0,
    // TODO
    favOpponent: '',
    hateOpponent: '',
    highestWin: { opponent: '', points: 0 },
    highestLoss: { opponent: '', points: 0 },
  };

  aroundStats = {
    aroundCount: 0,
    aroundMinCount: 0,
  };
  protected readonly Math = Math;

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.url.subscribe(url => {
      this.dataService.trainingData.subscribe(trainingsData => {
        // Ein Objekt, um die Zählung der Zahlen zu speichern
        const numberCount: any = {};
        const tripleCount: any = {};
        const user = trainingsData.users.find(user => user.name.toLowerCase() === url[1].path.toLowerCase());

        if (user) {
          this.training = user;
        }
        let sumAvg = 0;
        this.training.trainings.forEach(training => {
          if (training.darts.length % 3 === 0) {
            let sum = 0;
            for (const dart of training.darts.slice(-3)) {
              sum += dart.points;
            }
            if (sum > this.trainingStats.bestCheckout.points) {
              this.trainingStats.bestCheckout = { points: sum, checkout: training.darts.slice(-3) };
            }
          } else if (training.darts.length % 3 === 1) {
            let sum = 0;
            for (const dart of training.darts.slice(-1)) {
              sum += dart.points;
            }
            if (sum > this.trainingStats.bestCheckout.points) {
              this.trainingStats.bestCheckout = { points: sum, checkout: training.darts.slice(-1) };
            }
          } else if (training.darts.length % 3 === 2) {
            let sum = 0;
            for (const dart of training.darts.slice(-2)) {
              sum += dart.points;
            }
            if (sum > this.trainingStats.bestCheckout.points) {
              this.trainingStats.bestCheckout = { points: sum, checkout: training.darts.slice(-2) };
            }
          }

          // Anfang Gesamtanzahl der geworfenen Darts
          this.trainingStats.thrownDarts = this.trainingStats.thrownDarts + training.darts.length;
          // Ende Gesamtanzahl der geworfenen Darts

          // Anfang Minimum Darts
          if (this.trainingStats.minThrownDart === 0 || this.trainingStats.minThrownDart > training.darts.length) {
            this.trainingStats.minThrownDart = training.darts.length;
          }
          // Ende Minimum Darts

          // Anfang Maxmimum Darts
          if (this.trainingStats.maxThrownDart < training.darts.length) {
            this.trainingStats.maxThrownDart = training.darts.length;
          }
          // Ende Maxmimum Darts

          // Alle Durchschnitte summieren
          sumAvg = sumAvg + training.avg;

          // Über einzelne Würfe eines Trainings iterieren
          training.darts.forEach(dart => {
            // Anfang Lieblingszahl
            const number = dart.number;
            numberCount[number] = (numberCount[number] || 0) + 1;
            // Ende Lieblingszahl

            // Anfang Lieblingstripel
            if (dart.faktor === 3) {
              // Addiere den Faktor zur Gesamtsumme für die aktuelle Zahl
              const number = dart.number;
              tripleCount[number] = (tripleCount[number] || 0) + 1;
            }
            // Ende Lieblingstripel
          });
        });

        // Anfang Anzahl Gesamtspiele
        this.trainingStats.gamesOverall = this.training.trainings.length;
        // Ende Anzahl Gesamtspiele

        // Anfang Durchschnitt geworfene Darts
        this.trainingStats.thrownDartAvg = this.trainingStats.thrownDarts / this.trainingStats.gamesOverall;
        // Ende Durchschnitt geworfene Darts

        // Anfang Durchschnitt Punkte
        if (this.training.trainings.length > 0) {
          this.trainingStats.overallAvg = sumAvg / this.training?.trainings.length;
        }
        // Anfang Durchschnitt Punkte

        // Anfang Lieblingszahl
        for (const number in numberCount) {
          if (numberCount[number] > this.trainingStats.favNumberCount) {
            this.trainingStats.favNumber = +number;
            this.trainingStats.favNumberCount = numberCount[number];
          }
        }
        // Ende Lieblingszahl

        // Anfang Lieblingstripel
        for (const number in tripleCount) {
          if (tripleCount[number] > this.trainingStats.favTripleCount) {
            this.trainingStats.favTriple = +number;
            this.trainingStats.favTripleCount = tripleCount[number];
          }
        }
        // Ende Lieblingstripel
      });

      this.dataService.data.subscribe(data => {
        // Ein Objekt, um die Zählung der Zahlen zu speichern
        const numberCount: any = {};
        const tripleCount: any = {};
        const user = data.users.find(user => user.name.toLowerCase() === url[1].path.toLowerCase());

        if (user) {
          this.vs = user;
        }
        let sumAvg = 0;
        this.vs.games.forEach(game => {
          if (game.won) {
            this.vsStats.wins++;
          } else {
            this.vsStats.losses++;
          }

          if (game.darts.length % 3 === 0) {
            let sum = 0;
            for (const dart of game.darts.slice(-3)) {
              sum += dart.points;
            }
            if (sum > this.vsStats.bestCheckout.points) {
              this.vsStats.bestCheckout = { points: sum, checkout: game.darts.slice(-3) };
            }
          } else if (game.darts.length % 3 === 1) {
            let sum = 0;
            for (const dart of game.darts.slice(-1)) {
              sum += dart.points;
            }
            if (sum > this.vsStats.bestCheckout.points) {
              this.vsStats.bestCheckout = { points: sum, checkout: game.darts.slice(-1) };
            }
          } else if (game.darts.length % 3 === 2) {
            let sum = 0;
            for (const dart of game.darts.slice(-2)) {
              sum += dart.points;
            }
            if (sum > this.vsStats.bestCheckout.points) {
              this.vsStats.bestCheckout = { points: sum, checkout: game.darts.slice(-2) };
            }
          }

          // Anfang Gesamtanzahl der geworfenen Darts
          this.vsStats.thrownDarts = this.vsStats.thrownDarts + game.darts.length;
          // Ende Gesamtanzahl der geworfenen Darts

          // Anfang Minimum Darts
          if (this.vsStats.minThrownDart === 0 || this.vsStats.minThrownDart > game.darts.length) {
            this.vsStats.minThrownDart = game.darts.length;
          }
          // Ende Minimum Darts

          // Anfang Maxmimum Darts
          if (this.vsStats.maxThrownDart < game.darts.length) {
            this.vsStats.maxThrownDart = game.darts.length;
          }
          // Ende Maxmimum Darts

          // Alle Durchschnitte summieren
          sumAvg = sumAvg + game.avg;

          // Über einzelne Würfe eines Trainings iterieren
          game.darts.forEach(dart => {
            // Anfang Lieblingszahl
            const number = dart.number;
            numberCount[number] = (numberCount[number] || 0) + 1;
            // Ende Lieblingszahl

            // Anfang Lieblingstripel
            if (dart.faktor === 3) {
              // Addiere den Faktor zur Gesamtsumme für die aktuelle Zahl
              const number = dart.number;
              tripleCount[number] = (tripleCount[number] || 0) + 1;
            }
            // Ende Lieblingstripel
          });
        });
        // Anfang Anzahl Gesamtspiele
        this.vsStats.gamesOverall = this.vs.games.length;
        // Ende Anzahl Gesamtspiele

        // Anfang Durchschnitt geworfene Darts
        this.vsStats.thrownDartAvg = this.vsStats.thrownDarts / this.vsStats.gamesOverall;
        // Ende Durchschnitt geworfene Darts

        // Anfang Durchschnitt Punkte
        if (this.vs.games.length > 0) {
          this.vsStats.overallAvg = sumAvg / this.vs?.games.length;
        }
        // Anfang Durchschnitt Punkte

        // Anfang Lieblingszahl
        for (const number in numberCount) {
          if (numberCount[number] > this.vsStats.favNumberCount) {
            this.vsStats.favNumber = +number;
            this.vsStats.favNumberCount = numberCount[number];
          }
        }
        // Ende Lieblingszahl

        // Anfang Lieblingstripel
        for (const number in tripleCount) {
          if (tripleCount[number] > this.vsStats.favTripleCount) {
            this.vsStats.favTriple = +number;
            this.vsStats.favTripleCount = tripleCount[number];
          }
        }
        // Ende Lieblingstripel
      });

      this.dataService.aroundData.subscribe(aroundData => {
        const user = aroundData.users.find(user => user.name.toLowerCase() === url[1].path.toLowerCase());
        if (user) {
          this.aroundStats.aroundCount = user.games.length;

          user.games.forEach(game => {
            let sum = 0;
            game.forEach(number => {
              sum += number.count;
            });
            if (this.aroundStats.aroundMinCount === 0 || this.aroundStats.aroundMinCount > sum) {
              this.aroundStats.aroundMinCount = sum;
            }
          });
        }
      });
    });
  }
}
