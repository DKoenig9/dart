import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { BaseComponentsModule } from '@lenne.tech/ng-base/base-components';
import { BasePrototypeModule } from '@lenne.tech/ng-base/base-prototype';
import { SharedModule } from '../../modules/shared/shared.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { StatsComponent } from './pages/stats/stats.component';
import { NgChartsModule } from 'ng2-charts';
import { ReactiveFormsModule } from '@angular/forms';
import { GameSelectComponent } from './pages/game-select/game-select.component';
import { TrainingSelectComponent } from './pages/training-select/training-select.component';
import { TrainingComponent } from './pages/training/training.component';
import { StatsSelectComponent } from './pages/stats-select/stats-select.component';
import { AroundComponent } from './pages/around/around.component';
import { VsSelectComponent } from './pages/vs-select/vs-select.component';
import { VsComponent } from './pages/vs/vs.component';
import { AvgComponent } from './pages/avg/avg.component';

@NgModule({
  declarations: [
    MainComponent,
    DashboardComponent,
    StatsComponent,
    GameSelectComponent,
    TrainingSelectComponent,
    TrainingComponent,
    StatsSelectComponent,
    AroundComponent,
    VsSelectComponent,
    VsComponent,
    AvgComponent,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    BaseComponentsModule,
    BasePrototypeModule,
    SharedModule,
    NgChartsModule,
    ReactiveFormsModule,
  ],
})
export class MainModule {}
