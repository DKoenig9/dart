import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { MainComponent } from './main.component';
import { AuthGuard } from '@lenne.tech/ng-base';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { StatsComponent } from './pages/stats/stats.component';
import { GameSelectComponent } from './pages/game-select/game-select.component';
import { TrainingSelectComponent } from './pages/training-select/training-select.component';
import { TrainingComponent } from './pages/training/training.component';
import { StatsSelectComponent } from './pages/stats-select/stats-select.component';
import { AroundComponent } from './pages/around/around.component';
import { VsSelectComponent } from './pages/vs-select/vs-select.component';
import { VsComponent } from './pages/vs/vs.component';
import { AvgComponent } from './pages/avg/avg.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
        pathMatch: 'full',
      },
      {
        path: 'game',
        component: GameSelectComponent,
        pathMatch: 'full',
      },
      {
        path: 'game/vs',
        component: VsSelectComponent,
        pathMatch: 'full',
      },
      {
        path: 'game/vs/:id',
        component: VsComponent,
        pathMatch: 'full',
      },
      {
        path: 'game/training',
        component: TrainingSelectComponent,
        pathMatch: 'full',
      },
      {
        path: 'game/training/:id',
        component: TrainingComponent,
        pathMatch: 'full',
      },
      {
        path: 'game/around',
        component: AroundComponent,
        pathMatch: 'full',
      },
      {
        path: 'game/avg',
        component: AvgComponent,
        pathMatch: 'full',
      },
      {
        path: 'stats',
        component: StatsSelectComponent,
        pathMatch: 'full',
      },
      {
        path: 'stats/:id',
        component: StatsComponent,
        pathMatch: 'full',
      },
      {
        path: '**',
        redirectTo: 'stats',
      },

      {
        path: 'cms',
        loadChildren: () => import('./sections/cms/cms.module').then(m => m.CmsModule),
        canActivateChild: [AuthGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
